function addQueryToHistory(){
    historyEditor.insert(queryEditor.getValue() + "\n");
}

var queryEditor = ace.edit("queryString");
queryEditor.setTheme("ace/theme/xcode");
queryEditor.getSession().setMode("ace/mode/sql");
queryEditor.setValue('db.collection("' + $("#collectionName").text() + '").find()');

var historyEditor = ace.edit("queriesHistory");
historyEditor.setTheme("ace/theme/xcode");
historyEditor.setReadOnly(true);
historyEditor.getSession().setMode("ace/mode/sql");
addQueryToHistory();

function showJSON(){
    try {
        var input = eval('(' + $("#itemsString").text() + ')');
    }
    catch (error) {
        return alert("Cannot eval JSON: " + error);
    }
    var options = {
        collapsed : $('#collapsed').is(':checked'),
        withQuotes : $('#with-quotes').is(':checked')
    };
    $('#json-renderer').jsonViewer(input, options);
}

$(document).ready(function () {
    showJSON();
});

$('#collapsed').click(function(){
    showJSON();
});
$('#with-quotes').click(function () {
    showJSON();
});

function isQueryStringOK(queryString)
{
    if(queryString.indexOf("db.") > -1 && queryString.indexOf('db.collection("' + $("#collectionName").text() + '").' ) == -1)
    {
        return false;
    }
    return true;
}
function getQueryString(initialQuery){
    if(initialQuery.indexOf("db.collection") == -1)
    {
        return 'db.collection("' + $("#collectionName").text() + '").' + initialQuery;
    }
    return initialQuery;
}

$("#query-run").click(function(){
    addQueryToHistory();

    if(!isQueryStringOK(queryEditor.getValue()))
    {
        $("#errorAlert").show();return;
    }else{
        $("#errorAlert").hide();
    }
    var query = getQueryString(queryEditor.getValue());

    var allQueries = queryEditor.getValue().split(";\n");

    if(allQueries.length > 0)
    {
        var i = 0;
        var interval = setInterval(function(){
            $.ajax({
                url: "/connection/query/" + $("#connectionId").text(),
                data: {
                    query : getQueryString(allQueries[i])
                },
                success: function(data){
                    $("#itemsString").text(JSON.stringify(data));
                    showJSON();
                }
            });
            if(i == (allQueries.length - 1))
            {
                clearInterval(interval);
                alert("All queries finished.");
            }
            i++;
        }, 1000);
    }
    else{
        $.ajax({
            url: "/connection/query/" + $("#connectionId").text(),
            data: {
                query : query
            },
            success: function(data){
                $("#itemsString").text(JSON.stringify(data));
                showJSON();
            }
        });
    }

});

$("#clearQueriesHistory").click(function(){
    historyEditor.setValue("");
});
