# MongoIDE

MongoIDE is a node.js application that allows the user to manage MongoDB databases. It is built using node.js mongodb driver.


## Installation

Clone or download project.

Open project folder on terminal.

Install project dependencies using:

``` npm install ```


## Configuration

### Port Number

To change application listening port, change the value on _config/app.js_.

Port number default value: 3000



## Usage

### Start application
After installing all dependencies, initialize application using:

``` npm start ```

Or, if you have nodemon installed, using:

``` nodemon ./app.js ```


Access *localhost:port_number* on your browser, where port_number is the port the server is listening to.

### Create New Connection

1. Click on the button *New Connection* on the top navbar;
2. Fill in the new connection form;
    1. Connection Name - name of the connection being created
    2. Host - host location. Example: *localhost*
    3. Database - database name
    4. Username - username to connect to the host (*not required*)
    5. Password - password to connect to the host (*not required*)
    6. Port - port number of mongodb server (default value: *27017*)
3. Click on *Create* button

### Drop Connection

To drop a connection, you just need to click on the button *Delete* on the connection card under the dashboard.

### Create Collection

1. Open the connection where you want to create a new Collection
2. Fill in the *Collection Name* input on top of the page
3. Click on *Create Collection* button


### Drop Collection

To drop a collection, you just need to click on the button *Delete* on the desired collection card under the connection page.


### Run Queries

1. Open the collection in which you want to make the query
2. Under the section *Run Query*, write down the desired query
    - you can hide *db.collection("collection_name").* in the query
    - instead of *db.collection_name.*, you MUST use *db.collection("collection_name").*
3. Query result can be seen on the section *Result*

#### Run Multiple Queries

To run a sequence of queries, insert all of them in the Query Editor separated by ; and a break line.
When all queries are done, an alert message appears saying all queries were finished.
