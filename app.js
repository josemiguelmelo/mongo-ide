var express      = require('express');
var path         = require('path');
var favicon      = require('serve-favicon');
var logger       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var flash        = require('connect-flash');
var fs           = require('fs');


var app = express();

var connectionsData = "";
if (!fs.existsSync("config/connections.json")){
    fs.mkdirSync('config');
    fs.openSync("config/connections.json", 'w');
    fs.writeFileSync('config/connections.json', '{ "connections" : []}', 'utf8');
}
if (fs.existsSync("config/connections.json"))
    connectionsData = fs.readFileSync("config/connections.json", "utf8");

if (connectionsData == "") {
    connectionsData = '{ "connections" : []}';
}
var jsonConnections    = JSON.parse(connectionsData);
app.locals.connections = jsonConnections;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));
app.use(cookieParser('secret'));
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({secret : 'ilovescotchscotchyscotchscotch'}));
app.use(flash());

require('./routes/index')(app, fs);
require('./routes/users')(app);
require('./routes/connections')(app, fs);
//app.use('/', routes);
//app.use('/users', users);
//app.use('/connection', connections);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err    = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message : err.message,
            error : err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message : err.message,
        error : {}
    });
});

var appConfig = require("./config/app");
app.listen(appConfig.port_number, function () {
    console.log('MongoIDE listening on port ' + appConfig.port_number + '!');
});

module.exports = app;
