
var connectionUtils = require("../utils/connections");

var MongoClient = require('mongodb').MongoClient;


module.exports = function(app, fs) {

    /* GET users listing. */
    app.get('/connection/create', function(req, res, next) {
        res.render('connection/create', { title: 'New Connection', error:  req.flash('error'), success: req.flash('success') });
    });

    app.post('/connection/delete', function(req, res, next) {
        console.log(req.param("connectionID"));
        connectionUtils.removeConnection(fs, req.param("connectionID"));
        res.send(JSON.stringify({'error' : false, 'msg' : 'Connection deleted'}));
    });

    app.get('/connection/show/:connectionId', function(req, res, next) {
        var connectionId = req.params.connectionId;

        var connectionURL = connectionUtils.getConnectionUrlFromConnectionId(fs, connectionId);

        console.log(connectionURL)
        MongoClient.connect(connectionURL, function(err, db) {
            if (err != null) {
                console.log("Error connecting to mongo")
                return
            }
            
            console.log("Connected correctly to server");
            db.collections(function(err, collections){
                res.render('connection/show', {title : connectionId, connectionId : connectionId,
                    connectionURL : connectionURL, collections : collections});
                db.close();
            });
        });
    });

    app.get('/connection/query/:connectionId', function(req, res, next) {
        var connectionId = req.params.connectionId;
        var query = req.param("query");

        var connectionURL = connectionUtils.getConnectionUrlFromConnectionId(fs, connectionId);

        MongoClient.connect(connectionURL, function(err, db) {
            if (err != null) {
                console.log("Error connecting to mongo")
                return
            }

            console.log("Connected correctly to server");
            try{
                eval(query).toArray(function(err, items) {
                    if(err){
                        res.send(err);
                    }else{
                        res.send(items);
                    }
                    db.close();
                });
            }catch (err){
                if(query.indexOf("insert") > -1)
                {
                    res.send("Object added to collection.");
                }else if(query.indexOf("update") > -1){
                    res.send("Object updated in collection.");
                }else{
                    res.send(err.toString());
                }
                db.close();
            }
        });
    });


    app.get('/connection/show/:connectionId/:collectionName', function(req, res, next) {
        var connectionId = req.params.connectionId;
        var collectionName = req.params.collectionName;

        var connectionURL = connectionUtils.getConnectionUrlFromConnectionId(fs, connectionId);

        MongoClient.connect(connectionURL, function(err, db) {
            if (err != null) {
                console.log("Error connecting to mongo")
                return
            }
            console.log("Connected correctly to server");
            var collection = db.collection(collectionName);
            res.render('connection/collection/show', {title : connectionId, connectionId : connectionId,
                connectionURL : connectionURL, collection : collection, items: []});

        });
    });


    app.post('/collection/delete', function(req, res, next) {
        var connectionId = req.param('connection_name');
        var collectionName = req.param('collection_name');

        var connectionURL = connectionUtils.getConnectionUrlFromConnectionId(fs, connectionId);

        MongoClient.connect(connectionURL, function(err, db) {
            if (err != null) {
                console.log("Error connecting to mongo")
                return
            }
            console.log("Connected correctly to server");
            try{
                db.dropCollection(collectionName, function(err, collection){
                    if (err) {
                        res.send(JSON.stringify({'error' : true, 'msg' : 'Collection could not be deleted'}));
                    }
                    res.send(JSON.stringify({'error' : false, 'msg' : 'Collection deleted successfully'}));

                });
            }catch(err){
                res.send(JSON.stringify({'error' : true, 'msg' : 'Collection could not be deleted'}));
            }
        });
    });


    app.post('/collection/store', function(req, res, next) {
        var connectionId = req.param('connection_name');
        var collectionName = req.param('collection_name');

        var connectionURL = connectionUtils.getConnectionUrlFromConnectionId(fs, connectionId);

        MongoClient.connect(connectionURL, function(err, db) {
            if (err != null) {
                console.log("Error connecting to mongo")
                return
            }

            console.log("Connected correctly to server");
            db.createCollection(collectionName, function(err, collection){
                console.log(collection);
                if(err)
                {
                    res.send(JSON.stringify({'error' : true, 'msg' : 'Collection could not be created'}));
                }
                res.send(JSON.stringify({'error' : false, 'msg' : 'Collection created successfully'}));
            });
        });
    });


    app.post('/connection/store', function(req, res, next) {
        var identification = connectionUtils.generateIdentification(req.param('connection_name'));
        var port = req.param("connection_port");
        var host = req.param("connection_host");
        if(port == ""){
            port = "27017";
        }
        if(host == "" || host == "localhost"){
            host = "127.0.0.1";
        }

        var newConnection = {
            "identification" : identification,
            "name" : req.param("connection_name"),
            "host" : host,
            "username" : req.param("connection_username"),
            "password" : req.param("connection_password"),
            "database" : req.param("connection_database"),
            "port" : port
        };

        if(connectionUtils.containsConnection(fs, newConnection)){
            req.flash("error", 'Connection already exists.');
        }else{
            app.locals.connections = connectionUtils.addConnection(fs, newConnection);

            req.flash('success', 'Connection added successfully.')
        }
        res.redirect('/connection/create');

    });

};
