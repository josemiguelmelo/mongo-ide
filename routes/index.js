
var connectionUtils = require("../utils/connections");

var MongoClient = require('mongodb').MongoClient;


module.exports = function(app, fs) {
  /* GET home page. */
  app.get('/', function (req, res) {
    res.render('index', {title : 'MongoIDE', error : req.flash()});
  });
};