
var connectionsFileLocation = "config/connections.json";


module.exports = {
    generateIdentification : function(connectionName) {
        var identification = connectionName.toLowerCase();
        identification = identification.replace(" ", "_");
        return identification;
    },

    containsConnection : function (fs, connectionObj) {
        var allConnections = this.allConnections(fs);
        var i = 0;
        for( ; i < allConnections.connections.length; i++)
        {
            if(allConnections.connections[i].name == connectionObj.name || allConnections.connections[i].identification == connectionObj.identification)
                return true;
        }
        return false;
    },


    getConnectionUrl : function(connection) {
        var userConnection = "";
        if (connection.username != "" && connection.password != "")
        {
            userConnection = connection.username + ":" + connection.password  + "@";
        }
        var connectionURL = 'mongodb://' + userConnection + connection.host + ":" + connection.port + "/" + connection.database;
        return connectionURL;
    },



    getConnectionUrlFromConnectionId : function(fs, connectionId) {
        var connection = this.findConnection(fs, connectionId);

        return this.getConnectionUrl(connection);
    },

    removeConnection : function(fs, connectionID){
        var allCon = this.allConnections(fs);
        var allConnectionsFinal = { "connections" : []};
        for(var i = 0; i < allCon.connections.length; i++){
            if(allCon.connections[i].identification != connectionID)
                allConnectionsFinal.connections.push(allCon.connections[i]);
        }
        fs.writeFile(connectionsFileLocation, JSON.stringify(allConnectionsFinal), function(err){
            if (err) throw err;
        });
    },
    allConnections : function (fs){
        var connectionsData = fs.readFileSync(connectionsFileLocation, "utf8");
        return JSON.parse(connectionsData);
    },
    findConnection : function (fs, connectionId){
        var allConnections = this.allConnections(fs);

        var i = 0;
        for( ; i < allConnections.connections.length; i++)
        {
            if(allConnections.connections[i].identification == connectionId)
                return allConnections.connections[i];
        }
        return null;
    },

    addConnection : function(fs, newConnection)
    {
        var allConnections = this.allConnections(fs);

        allConnections.connections.push( newConnection );

        fs.writeFile(connectionsFileLocation, JSON.stringify(allConnections), function(err){
            if (err) throw err;
        });

        return allConnections;
    }


};
